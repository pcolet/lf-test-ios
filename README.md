# TODO

Le test a pour but de constuire une application iOS en **Swift** avec une **architecture propre**, compatible  **iOS 9+** et respectant le standard **autolayout**, **sans .xib ni .storyboard** (Stevia).
L'application se compose d'une seule fiche restaurant.


## La Fiche

- La fiche restaurant doit être une collectionView.
- Les cellules respectent le design du screenshot.
- Les données du restaurant sont accessible notre [API](https://api.lafourchette.com/api?key=IPHONEPRODEDCRFV&method=restaurant_get_info&id_restaurant=6861)
- D'autres restaurants ? (ex : 40370, 16409, 14163).
- Les tests unitaires sont les bienvenus.


![picture](https://s3-eu-west-1.amazonaws.com/lf-mobile-asset/fiche-restaurant_ios.png)


## Bonus :

- Tests fonctionnels
- Compatibilité iPad
- Gestion du mode Offline
- ¿ Apple Watch ?

# BON COURAGE